# Purpose

I needed to see what powershell scripts are really doing. I didn't find any unix shell -x flag equivalent. Then I wrote this small script. It adds some extra powershell lines into source script to print how interesting functions are invocked and with which arguments.

# Usage

Some powershell code statements could be broken into multiple lines. Firstly it needs to be unwind.
```
$ find . -name '*ps1' -exec ./pwsh-unbreak {} \;
```

List of interesting functions have to be prepared
```
$ cat > ./pwsh-x--interesting-functions.txt <<EOF
interesting_function_name
another_interesting_function_name
EOF
```

Then it could be tried to insert extra debug messages
```
find . -name '*ps1' -exec ./pwsh-x {} \;
```

Now run your main script and cross your fingers ;)

# Limitations

It works fine for specific project I take part in. It is not designed as general purpose tool. It is based on same regular expression processing instead of analyzing sytax tree of powershell script.

# Proper way
Probably there are some nice trace functions in powershell itself. I didn't know about them and found any information about them before these scripts are done. I didn't test them so far. Then I advice to look at them.

https://docs.microsoft.com/en-us/powershell/module/Microsoft.PowerShell.Utility/Trace-Command

https://docs.microsoft.com/en-us/powershell/module/Microsoft.PowerShell.Utility/set-tracesource

# Powershell runtime
Powershell virtual machine creates lots of unnescessary threads even when running script to print 'hello world' message.

https://github.com/PowerShell/PowerShell/issues/9462
